import click
import getpass
import pyperclip

from .cli_utils import get_selected_option
from .config import Config
from .database import DatabaseManager
from .pasto import Pasto

config = Config()
db = DatabaseManager(config.db_path)


@click.group(invoke_without_command=True)
@click.option('--user', '-u', is_flag=True)
@click.option('--password', '-p', is_flag=True)
@click.option('--url', is_flag=True, help='Entry web address.')
@click.argument('entry')
def main(**args):
    entry = args.pop('entry')
    option = get_selected_option(**args)

    db_pass = getpass.getpass('Master password: ')
    pasto = Pasto(db, db_pass)
    entry_component = pasto.get_entry_component(entry, option)
    pyperclip.copy(entry_component)


if __name__ == "__main__":
    main()
