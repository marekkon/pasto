import os
import os.path
from strictyaml import load as yaml_load, Map, Str, Optional


class Config():

    config = {
        'database': {
            'path': os.path.join(os.environ['HOME'], '.local', 'share', 'pasto'),
            'name': 'pasto.db'
        }
    }

    config_path = os.environ.get('XDG_CONFIG_HOME', os.path.join(os.environ['HOME'], '.config'))
    config_file = os.path.join(config_path, 'pasto.yml')

    KEY_SEPARATOR = '.'


    def load(self):
        schema = Map({Optional('database'): Map({Optional('path'): Str(), Optional('name'): Str()})})

        try:
            with open(self.config_file) as file:
                data = file.read()
                user_config = yaml_load(data, schema).data
                self.config.update(user_config)

        except FileNotFoundError:
            pass


    def get(self, item):
        if self.KEY_SEPARATOR not in item:
            return self.config[item]

        keys = item.split(self.KEY_SEPARATOR)
        config_item = self.config
        for key in keys:
            config_item = config_item.get(key)

        return config_item
