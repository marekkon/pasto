def get_selected_option(**args):
    selected = False
    for arg in args.keys():
        if args[arg]:
            if selected:
                raise TypeError('One and only one option is required')
            selected = arg

    if not selected:
        raise TypeError('One and only one option is required')

    return selected
